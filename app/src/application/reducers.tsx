import {
    action,
    appState,
} from './types'

const initialAppState: appState = {
    data: {},
    loading: 0,
}

export function reducer(state = initialAppState, action: action): appState {
    switch (action.type) {
        case 'LOADING':
            return {
                ...state,
                loading: state.loading + 1,
            }

        case 'FETCH_DATA_SUCCESS':
            return {
                ...state,
                data: {
                    ...state.data,
                    [action.value.key]: {
                        ...action.value.response,
                        loadedTime: Date.now(),
                    }
                },
                loading: state.loading - 1,
            }

        case 'FETCH_FAILURE':
            return {
                ...state,
                loading: state.loading - 1,
            }

        default:
            return state
    }
}
