import * as redux from 'redux'

export interface appState {
    data: any,
    loading: number,
    dispatch?: redux.Dispatch<any>,
}

export interface action {
    type: string,
    promise?: (dispatch: any, getState: any) => any,
    value?: any,
}
