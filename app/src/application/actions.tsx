import {
    action,
} from './types'

import config from './config'
const ESServerUrl = config('app').ESServerUrl

export function fetchData(stateKey: string, fetchProperties: any): action {
    return {
        type: 'FETCH_DATA',
        promise: (dispatch, getState) => {
            dispatch(loading())

            let {distantUrl, url, responseIsJson, body} = fetchProperties

            let fetchUrl = distantUrl ?
                distantUrl + url :
                ESServerUrl + url

            let fbody = JSON.stringify(body)

            fetch(
                fetchUrl,
                {
                    'method': fbody ? 'POST' : 'GET',
                    'body': fbody,
                    'headers': {
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json',
                    },
                }
            ).then((res: any) => {
                return res.json()
            }).then( (json: any) => {
                dispatch(receivedData(stateKey, json))
            }).catch( (err: any) => {
                console.log(err)
                dispatch(fetchFailed(err))
            })
        },
    }
}

export function loading(): action {
    return {
        type: 'LOADING',
    }
}

export function receivedData(stateKey: any, response: any): action {
    return {
        type: 'FETCH_DATA_SUCCESS',
        value: {
            key: stateKey,
            response,
        }
    }
}

export function fetchFailed(err: any): action {
    return {
        type: 'FETCH_FAILURE',
        value: err,
    }
}
