import {
    Tab,
    Tabs,
    TabId,
    Icon,
} from '@blueprintjs/core'
import * as React from 'react'
import {Route} from 'react-router'
import {BrowserRouter} from 'react-router-dom'
import {connect} from 'react-redux'
// import DatePicker from 'react-datepicker'
// import DayPicker from 'react-day-picker'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import * as moment from 'moment'

import {
    appState,
} from '../types'
import * as actions from '../actions'

// import 'react-datepicker/dist/react-datepicker.css'
import 'react-day-picker/lib/style.css';
import './app.less'

const mapReduxStateToReactProps = (state : appState): appState => {
    return state
}

function reduxify(mapReduxStateToReactProps: any, mapDispatchToProps?: any, mergeProps?: any, options?: any) {
    return (target: any) => (connect(mapReduxStateToReactProps, mapDispatchToProps, mergeProps, options)(target) as any)
}

interface State {
    currentDate: any,
    currentSearch: string,
    activeTabId?: TabId;
    activePanelOnly?: boolean;
    animate?: boolean;
    navbarTabId?: TabId;
    vertical?: boolean;
    esServerUrl: string;
}

@reduxify(mapReduxStateToReactProps)
export class App extends React.Component<appState, State> {
    constructor(props: appState) {
        super(props)
        this.state = {
            currentDate: null,
            currentSearch: null,
            activePanelOnly: false,
            animate: true,
            navbarTabId: 'Home',
            vertical: false,
            esServerUrl: 'http://localhost:9200/',
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.fetchData(
            'summaryDates',
            {
                url: 'summary/_search/',
                responseIsJson: true,
                body: {
                    query: {
                        match_all: {}
                    },
                    _source: ['DATE_PUBLI'],
                    size: 100,
                    sort: {
                        DATE_PUBLI: {
                            order: 'desc'
                        }
                    },
                },
                distantUrl: this.state.esServerUrl,
            })
        )
    }

    private handleTabChange = (activeTabId: TabId) => this.setState({ activeTabId });

    handleSummaryDateChange(date: any) {
        if (date && (!this.state.currentDate || date.getTime() != this.state.currentDate.getTime())) {
            this.setState({
                currentDate: date,
            })

            let queryDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-')

            this.props.dispatch(actions.fetchData(
                'summary',
                {
                    'url':'summary/_search/',
                    'responseIsJson': true,
                    'body': {
                        'query': {
                            'match': {
                                'DATE_PUBLI': queryDate,
                            }
                        }
                    },
                    'distantUrl': this.state.esServerUrl
                }
            ))
        }
    }

    handleArticleClick(href: string, varName: string) {
        let cid = /cidTexte\=(.[A-Z0-9]*)\&/g.exec(href)[1]

        this.props.dispatch(actions.fetchData(
            varName,
            {
                'url':'article/_search/',
                'responseIsJson': true,
                'body': {
                    'query': {
                        'match': {
                            'ID': cid,
                        }
                    }
                },
                'distantUrl': this.state.esServerUrl
            }
        ))
    }

    handleSearchBarKeyPress(event: any) {
        if(event.key == 'Enter'){
            this.triggerSearch()
        }
    }

    triggerSearch() {
        if (this.state.currentSearch && this.state.currentSearch != '') {
            this.props.dispatch(actions.fetchData(
                'articlesText',
                {
                    'url':'article/_search/?size=5',
                    'responseIsJson': true,
                    'body': {
                        'query': {
                          "nested": {
                            "path": "STRUCT.articles",
                            "query": {
                              "bool": {
                                "must": [
                                  { "match": { "STRUCT.articles.BLOC_TEXTUEL": this.state.currentSearch }}
                                ]
                              }
                            }
                          }
                        }
                    },
                    'distantUrl': this.state.esServerUrl
                }
            ))
        }
    }

    updateSearch(event: any) {
        this.setState({
            currentSearch: event.target.value,
        })
    }

    buildHierarchy(articles: any) {
        let root: any = {
            'name': 'root',
            'children': [],
            'articles': [],
        }

        for (let i = 0; i < articles.length; i++) {
            let href = articles[i].href
            let text = articles[i].text
            let parts = articles[i].path
            let currentNode = root

            for (let j = 0; j < parts.length; j++) {
                let children = currentNode['children']
                let articles = currentNode['articles']
                let nodeName = parts[j]
                let childNode

                let foundChild = false
                for (let k = 0; k < children.length; k++) {
                    if (children[k]['name'] == nodeName) {
                        childNode = children[k]
                        foundChild = true
                        break
                    }
                }
                if (!foundChild) {
                    childNode = {'name': nodeName, 'children': [], 'articles': []}
                    children.push(childNode)
                }
                currentNode = childNode
            }

            let childNode = {'href': href, 'text': text}
            currentNode.articles.push(childNode)
        }

        return root
    }

    buildRecursiveLi(depth: number, node: any) {
        let title

        if (depth == 0) {
            title = <h2>{node.name}</h2>
        } else if (depth == 1) {
            title = <h3>{node.name}</h3>
        } else if (depth == 2) {
            title = <h4>{node.name}</h4>
        } else if (depth == 3) {
            title = <h5>{node.name}</h5>
        } else if (depth == 1){
            title = <h6>{node.name}</h6>
        } else {
            title = <h6>{node.name}</h6>
        }

        if (node.children) {
            return <li key={node.name}>
                    {title}
                    <ul>
                        {node.articles.map(this.buildRecursiveLi.bind(this, depth))}
                    </ul>
                    <ul>
                        {node.children.map(this.buildRecursiveLi.bind(this, depth+1))}
                    </ul>
                </li>
        } else {
            return <li
                className={'article'}
                onClick={() => this.handleArticleClick(node.href, 'articleParution')}
                key={node.href}>
                    <span className={'result'}>{node.text}</span>
            </li>
        }
    }

    enrichText(links: any, text: string) {
        console.log(text)
        links.forEach((link: any) => {
            text = text.replace('parsedLink#' + link.hash, '<a href="http://legifrance.gouv.fr' + link.href + '">' + link.text + '</a>')
        })
        return this.nl2br(text)
    }

    nl2br(text: string) {
        return text.replace(/\n/g,'<br />')
    }

    render () {
        let {data, loading} = this.props

        const MONTHS = [
            'Janvier',
            'Février',
            'Mars',
            'Avril',
            'Mai',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre',
        ]

        const WEEKDAYS_LONG = [
            'Dimanche',
            'Lundi',
            'Mardi',
            'Mercredi',
            'Jeudi',
            'Vendredi',
            'Samedi',
        ]

        const WEEKDAYS_SHORT = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa']

        // Get summary dates if available
        let summaryDates = (data && data.summaryDates && data.summaryDates.hits.hits) ?
            data.summaryDates.hits.hits.map((hit: any) => {
                let info = hit._source.date.split('-')
                return new Date(info[0], info[1]-1, info[2])
            }) :
            []

        // Get searched articles' link if available
        let searchedArticlesLi = (data && data.articlesText && data.articlesText.hits.hits) ?
            data.articlesText.hits.hits.map((hit: any) => {
                let article = hit._source
                return <li
                    className={'article'}
                    onClick={() => this.handleArticleClick(article.url, 'articleText')}
                    key={article.url}>
                        <span className={'result'}>{article.summaryText}</span>
                </li>
            }) :
            []

        // Get summary articles' link if available
        let summaryArticles = (data && data.summary && data.summary.hits.hits[0]) ?
            data.summary.hits.hits[0]._source.array :
            []

        const hierarchy = (summaryArticles.length > 0) ?
            this.buildHierarchy(summaryArticles) :
            {}

        let summaryArticlesLi = (hierarchy.children) ?
            hierarchy.children.map(this.buildRecursiveLi.bind(this, 0)) :
            null

        // Get article data if available
        let entete: any = {
            parution: null,
            text: null
        }
        let corps: any = {
            parution: null,
            text: null
        }

        let article = (data && data.articleParution && data.articleParution.hits.hits[0]) ?
            data.articleParution.hits.hits[0]._source :
            null

        entete['parution'] = article ?
            this.enrichText(article.links, article.entete) :
            null

        corps['parution'] = article ?
            this.enrichText(article.links, article.article) :
            null

        let articleText = (data && data.articleText && data.articleText.hits.hits[0]) ?
            data.articleText.hits.hits[0]._source :
            null

        entete['text'] = articleText ?
            this.enrichText(articleText.links, articleText.entete) :
            null

        corps['text'] = articleText ?
            this.enrichText(articleText.links, articleText.article) :
            null

        let dateTab = <div id='parutionGrid'>
            <div id='searchParution'>
                <div className='input-group small'>
                    <div className='icon'>
                        Date de Parution
                    </div>
                    <div className='right-arrow'></div>
                    <DayPickerInput
                        onDayChange={this.handleSummaryDateChange.bind(this)}
                        dayPickerProps={{
                            modifiers: {
                                // disabled: { daysOfWeek: [0] },
                                available: summaryDates,
                            },
                            locale: 'fr',
                            firstDayOfWeek: 1,
                            months: MONTHS,
                            weekdaysLong: WEEKDAYS_LONG,
                            weekdaysShort: WEEKDAYS_SHORT,
                        }}
                    />
                </div>
            </div>
            <div id='summaryParution'>
                <ul>
                    {summaryArticlesLi}
                </ul>
            </div>
            <div id='articleParution'>
                <div className='entete' dangerouslySetInnerHTML={{__html: entete['parution']}}></div>
                <br/>
                <div className='corps' dangerouslySetInnerHTML={{__html: corps['parution']}}></div>
            </div>
        </div>

        let save = <div>
            <input
                placeholder='Recherche textuelle'
                onChange={this.updateSearch.bind(this)}
            />
            <button onClick={this.triggerSearch.bind(this)}>Rechercher</button>
        </div>

        let textTab = <div id='textGrid'>
            <div id='searchText'>
                <div className='input-group'>
                    <div className='icon'>
                        <Icon icon='search'/>
                    </div>
                    <div className='right-arrow'></div>
                    <input
                        placeholder='Recherche textuelle'
                        onChange={this.updateSearch.bind(this)}
                        onKeyPress={this.handleSearchBarKeyPress.bind(this)}
                    />
                    <button onClick={this.triggerSearch.bind(this)}>Rechercher</button>
                </div>
            </div>
            <div id='summaryText'>
                <ul>
                    {searchedArticlesLi}
                </ul>
            </div>
            <div id='articleText'>
                <div className='entete' dangerouslySetInnerHTML={{__html: entete['text']}}></div>
                <br/>
                <div className='corps' dangerouslySetInnerHTML={{__html: corps['text']}}></div>
            </div>
        </div>

        return (
            <div id='main'>
                <div id='header'>
                    <h1>Journal Officieux</h1>
                    <Tabs id="searchTabs" onChange={this.handleTabChange} selectedTabId={this.state.activeTabId}>
                        <Tab id="text" title="Recherche Textuelle" panel={textTab} />
                        <Tab id="date" title="Recherche par Parution" panel={dateTab} />
                    </Tabs>
                </div>
            </div>
        )
    }
}
