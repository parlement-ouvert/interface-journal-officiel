import {
    Tab,
    Tabs,
    TabId,
    Icon,
} from '@blueprintjs/core'
import * as React from 'react'
import {Route} from 'react-router'
import {BrowserRouter} from 'react-router-dom'
import {connect} from 'react-redux'

import {
    appState,
} from '../types'
import * as actions from '../actions'

import './apropos.less'

import config from './../config'

const mapReduxStateToReactProps = (state : appState): appState => {
    return state
}

function reduxify(mapReduxStateToReactProps: any, mapDispatchToProps?: any, mergeProps?: any, options?: any) {
    return (target: any) => (connect(mapReduxStateToReactProps, mapDispatchToProps, mergeProps, options)(target) as any)
}

@reduxify(mapReduxStateToReactProps)
export class Apropos extends React.Component<appState, any> {
    constructor(props: appState) {
        super(props)
    }

    render () {
        return (
            <div id='apropos'>
                <a href='/'><h1>Le Journal Officieux</h1></a>

                <p>
                    Cette plateforme a pour but de rendre plus accessible l'information du Journal Officiel français
                    mise à disposition par la Direction de l'Information Légale et Administrative (DILA) sur
                    son <a href='http://echanges.dila.gouv.fr/OPENDATA/'>serveur FTP</a>.<br/>
                    Elle est hébergée sur un serveur personnel.
                </p>

                <h2>Avertissement</h2>
                <p>
                    Elle n'est pas reconnue par quelqu'instance officielle; par ailleurs,
                    elle ne garantit pas l'exactitude ou l'exhaustivité des données présentées. Si en l'occurrence vous
                    pensez qu'il y a des données manquantes ou mal affichées, vous pouvez écrire un message à l'adresse
                    mail indiquée plus bas.
                </p>

                <h2>Licence</h2>
                <p>
                    Les deux sous-projets qui composent cette application sont sous licence <a href='https://www.gnu.org/licenses/agpl-3.0.en.html'>AGPL v3</a>.
                </p>

                <h2>Contributions</h2>
                <p>
                    Le projet actuel se décompose en deux sous-projets auxquels il est possible de contribuer librement :
                    <ul>
                        <li>
                            La collecte des fichiers XML présents sur le serveur de la DILA et le stockage de ces données
                            sur une instance ElasticSearch.<br/>
                            Repo git : <a href='https://github.com/alexis-thual/parsing-journal-officiel'>https://github.com/alexis-thual/parsing-journal-officiel</a>
                        </li>
                        <li>
                            L'affichage des fichiers collectés dans une interface web développée avec React, Redux et BlueprintJS.<br/>
                            Repo git : <a href='https://framagit.org/parlement-ouvert/interface-journal-officiel'>https://framagit.org/parlement-ouvert/interface-journal-officiel</a>
                        </li>
                    </ul>
                </p>

                <h2>Contact</h2>
                <p>
                    Pour toute prise de contact, vous pouvez écrier à l'adresse suivante :<br/>
                    alexisthual (at) gmail (point) com
                </p>
            </div>
        )
    }
}
