import {
    Tab,
    Tabs,
    TabId,
    Icon,
} from '@blueprintjs/core'
import * as React from 'react'
import {Route} from 'react-router'
import {BrowserRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import * as moment from 'moment'

import {
    appState,
} from '../types'
import * as actions from '../actions'

import 'react-day-picker/lib/style.css'
import './app.less'

import config from './../config'

const mapReduxStateToReactProps = (state : appState): appState => {
    return state
}

function reduxify(mapReduxStateToReactProps: any, mapDispatchToProps?: any, mergeProps?: any, options?: any) {
    return (target: any) => (connect(mapReduxStateToReactProps, mapDispatchToProps, mergeProps, options)(target) as any)
}

interface State {
    currentDate: any,
    currentSearch: string,
    activeTabId?: TabId;
    activePanelOnly?: boolean;
    animate?: boolean;
    navbarTabId?: TabId;
    vertical?: boolean;
    ESServerUrl: string;
    sortBy: string;
    currentResultPage: number;
    resultsPerPage: number;
    currentArticleSearchIndex: number;
}

@reduxify(mapReduxStateToReactProps)
export class App extends React.Component<appState, State> {
    summaryDates: any

    constructor(props: appState) {
        super(props)
        this.state = {
            currentDate: null,
            currentSearch: null,
            activePanelOnly: false,
            animate: true,
            activeTabId: 'date',
            vertical: false,
            ESServerUrl: config('app').ESServerUrl,
            sortBy: '0',
            currentResultPage: 0,
            resultsPerPage: 10,
            currentArticleSearchIndex: null,
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.fetchData(
            'summaryDates',
            {
                url: 'summary/_search/',
                responseIsJson: true,
                body: {
                    query: {
                        range: {
                            DATE_PUBLI: {
                                gte: 'now/M',
                                lte: 'now/M',
                            }
                        }
                    },
                    _source: ['DATE_PUBLI'],
                    size: 31,
                    sort: {
                        DATE_PUBLI: {
                            order: 'desc',
                        }
                    },
                },
                distantUrl: this.state.ESServerUrl,
            })
        )
    }

    private handleTabChange = (activeTabId: TabId) => this.setState({ activeTabId });

    handleSummaryDateChange(date: any) {
        if (date && (!this.state.currentDate || date.getTime() != this.state.currentDate.getTime())) {
            this.setState({
                currentDate: date,
            })

            let queryDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-')

            this.props.dispatch(actions.fetchData(
                'summary',
                {
                    'url':'summary/_search/',
                    'responseIsJson': true,
                    'body': {
                        'query': {
                            'match': {
                                'DATE_PUBLI': queryDate,
                            }
                        }
                    },
                    'distantUrl': this.state.ESServerUrl
                }
            ))
        }
    }

    handleArticleClick(id: string, varName: string) {
        this.props.dispatch(actions.fetchData(
            varName,
            {
                'url':'article/_search/',
                'responseIsJson': true,
                'body': {
                    'query': {
                        'match': {
                            'ID': id,
                        }
                    }
                },
                'distantUrl': this.state.ESServerUrl
            }
        ))
    }

    handleTextArticleClick(index: number) {
        this.setState({
            currentArticleSearchIndex: index,
        })
    }

    handleSearchBarKeyPress(event: any) {
        if(event.key == 'Enter'){
            this.triggerSearch(true)
        }
    }

    triggerSearch(reset: boolean) {
        let {resultsPerPage, currentResultPage, currentSearch} = this.state

        let callback = () => {
            if (currentSearch && currentSearch != '') {
                let query: any = {
                    'query': {
                        'nested': {
                            'path': 'STRUCT.articles',
                            'query': {
                                'query_string': {
                                    'default_field': 'STRUCT.articles.BLOC_TEXTUEL',
                                    'default_operator': 'AND',
                                    'query': currentSearch
                                }
                            },
                            'inner_hits': {
                                'highlight': {
                                    'fields': {
                                        'STRUCT.articles.BLOC_TEXTUEL': {
                                            'pre_tags': ['<em class=\"highlight\">'],
                                            'post_tags': ['</em>'],
                                            'number_of_fragments': 0
                                        }
                                    }
                                }
                            }
                        }
                    },
                    'highlight': {
                        'require_field_match': false,
                        'fields': {
                            'TITREFULL': {
                                'pre_tags': ['<em class=\"highlight\">'],
                                'post_tags': ['</em>'],
                                'force_source': true
                            }
                        }
                    },
                    'from': currentResultPage * resultsPerPage,
                    'size': resultsPerPage
                }

                switch (this.state.sortBy) {
                    case '0':
                        break
                    case '1':
                        query['sort'] = {
                            'DATE_PUBLI': 'desc'
                        }
                        break
                    case '2':
                        query['sort'] = {
                            'DATE_PUBLI': 'asc'
                        }
                        break
                    default:
                        break
                }

                this.props.dispatch(actions.fetchData(
                    'articlesText',
                    {
                        'url': 'article/_search',
                        'responseIsJson': true,
                        'body': query,
                        'distantUrl': this.state.ESServerUrl
                    }
                ))
            }
        }

        if (reset) {
            this.setState({
                currentResultPage: 0,
            }, callback)
        } else {
            callback()
        }

    }

    updateSearch(event: any) {
        this.setState({
            currentSearch: event.target.value,
        })
    }

    buildRecursiveSummaryStructure(depth: number, node: any) {
        let title = (() => {
            switch (depth) {
                case 0: return <h2>{node.name}</h2>;
                case 1: return <h3>{node.name}</h3>;
                case 2: return <h4>{node.name}</h4>;
                case 3: return <h5>{node.name}</h5>;
            }
        })()

        // The root node is here for facilitating storate
        // but is ill-named.
        if (node.name == 'root') {
            title = null
        }

        return <li key={node.name}>
            {title}
            <ul className='articles'>
                {node.links.map((link: any) =>
                    <li
                        className={'article'}
                        onClick={() => this.handleArticleClick(link.idtxt, 'articleDate')}
                        key={link.idtxt}>
                            <span className={'result'}>{link.titre}</span>
                    </li>)}
            </ul>
            <ul className='nodes'>
                {node.children.map(this.buildRecursiveSummaryStructure.bind(this, depth+1))}
            </ul>
        </li>

    }

    parseArticle(article: any) {
        let offsets: number[] = []
        let highlightedArticles: any = []

        if (article.inner_hits) {
            article.inner_hits['STRUCT.articles'].hits.hits.forEach((hit: any) => {
                offsets.push(hit._nested.offset)
                highlightedArticles.push(hit.highlight['STRUCT.articles.BLOC_TEXTUEL'][0])
            })
        }

        return <div>
            <div className={'entete'}>
                <h3 dangerouslySetInnerHTML={{__html: article._source.highlight ? article._source.highlight.TITREFULL : article._source.TITREFULL}}/>
                <div>ELI: {article._source.ID_ELI}</div>
                <div>NOR: {article._source.NOR}</div>
            </div>
            <div className={'corps'}>
                <div dangerouslySetInnerHTML={{__html: article._source.NOTICE}}/>
                <div dangerouslySetInnerHTML={{__html: article._source.VISAS}}/>
                <div dangerouslySetInnerHTML={{__html: article._source.ABRO}}/>
                <div dangerouslySetInnerHTML={{__html: article._source.RECT}}/>
                <div dangerouslySetInnerHTML={{__html: article._source.SM}}/>
                <div dangerouslySetInnerHTML={{__html: article._source.TP}}/>
                {article._source.STRUCT.articles.map((a: any, index: number) => {
                        let indexOffset = (offsets.length > 0) ? offsets.indexOf(index) : -1
                        return <div key={article.ID}>
                            <div className={'articleHeader'}>
                                <span>Article {index + 1}</span>
                            </div>
                            <span dangerouslySetInnerHTML={{__html: (indexOffset != -1) ? highlightedArticles[indexOffset] : a.BLOC_TEXTUEL}}/>
                        </div>
                    }
                )}
                <div dangerouslySetInnerHTML={{__html: article._source.STRUCT.signataires}}/>
            </div>
        </div>
    }

    dayIsAvailable(day: any) {
        let date = moment(day).format('YYYY-MM-DD')
        return this.summaryDates.indexOf(date) != -1
    }

    dayIsDisabled(day: any) {
        return !this.dayIsAvailable(day)
    }

    sortByChange(event: any) {
        this.setState({
            sortBy: event.target.value,
        }, () => this.triggerSearch(true))
    }

    dayPickerMonthChange(date: any) {
        let d = moment(date).format('YYYY-MM-DD')

        this.props.dispatch(actions.fetchData(
            'summaryDates',
            {
                url: 'summary/_search/',
                responseIsJson: true,
                body: {
                    query: {
                        range: {
                            DATE_PUBLI: {
                                gte: d + '||/M',
                                lte: d + '||/M',
                            }
                        }
                    },
                    _source: ['DATE_PUBLI'],
                    size: 31,
                    sort: {
                        DATE_PUBLI: {
                            order: 'desc'
                        }
                    },
                },
                distantUrl: this.state.ESServerUrl,
            })
        )
    }

    changeResultPage(direction: string) {
        let currentResultPage = this.state.currentResultPage
        switch(direction) {
            case 'prev':
                if (this.state.currentResultPage > 0) {
                    this.setState({
                        currentResultPage: currentResultPage - 1
                    }, () => {
                        this.triggerSearch(false)
                    })
                }
                break
            case 'next':
                this.setState({
                    currentResultPage: currentResultPage + 1
                }, () => {
                    this.triggerSearch(false)
                })
                break
            default:
                break
        }
    }

    render () {
        let {data, loading} = this.props
        let {activeTabId, currentResultPage, currentArticleSearchIndex} = this.state

        const MONTHS = [
            'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
            'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
        ]

        const WEEKDAYS_LONG = [
            'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'
        ]

        const WEEKDAYS_SHORT = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa']

        // Get summary dates if available
        this.summaryDates = (data && data.summaryDates && data.summaryDates.hits.hits) ?
            data.summaryDates.hits.hits.map((hit: any) => hit._source.DATE_PUBLI) :
            []

        let dateTab = <div className='input-group small'>
                <div className='icon'>
                    Date de Parution
                </div>
                <div className='right-arrow'></div>
                <DayPickerInput
                    onDayChange={this.handleSummaryDateChange.bind(this)}
                    dayPickerProps={{
                        onMonthChange: this.dayPickerMonthChange.bind(this),
                        modifiers: {
                            available: this.dayIsAvailable.bind(this),
                            disabled: this.dayIsDisabled.bind(this)
                        },
                        locale: 'fr',
                        firstDayOfWeek: 1,
                        months: MONTHS,
                        weekdaysLong: WEEKDAYS_LONG,
                        weekdaysShort: WEEKDAYS_SHORT,
                    }}
                />
            </div>

        let searchTab = <div id='textSearchBar' className='flex-box'>
            <div className='input-group'>
                <div className='icon'>
                    <Icon icon='search'/>
                </div>
                <div className='right-arrow'></div>
                <input
                    placeholder='Recherche textuelle'
                    onChange={this.updateSearch.bind(this)}
                    onKeyPress={this.handleSearchBarKeyPress.bind(this)}
                />
                <button onClick={this.triggerSearch.bind(this, true)}>Rechercher</button>
            </div>
            <label className={'pt-label pt-inline'}>
                Trier Par
                <div className={'pt-select'}>
                    <select onChange={this.sortByChange.bind(this)}>
                        <option value='0'>Pertinence</option>
                        <option value='1'>Date décroissante</option>
                        <option value='2'>Date croissante</option>
                    </select>
                </div>
            </label>
        </div>

        let articlesListDiv, articleDiv, articlesListArrowsDiv

        switch(activeTabId) {
            case 'date':
                articlesListDiv = (data && data.summary && data.summary.hits.hits[0]) ?
                    this.buildRecursiveSummaryStructure(0, data.summary.hits.hits[0]._source.STRUCTURE_TXT) :
                    null
                articleDiv = (data && data.articleDate && data.articleDate.hits.hits[0]) ?
                    this.parseArticle(data.articleDate.hits.hits[0]) :
                    null
                articlesListArrowsDiv = null
                break
            case 'search':
                articlesListDiv = (data && data.articlesText && data.articlesText.hits.hits) ?
                    data.articlesText.hits.hits.map((a: any, index: number) =>
                        <li
                            className={'article'}
                            // onClick={() => this.handleArticleClick(a._source.ID, 'articleSearch')}
                            onClick={() => this.handleTextArticleClick(index)}
                            key={a._source.ID}>
                                <span>{a._source.DATE_PUBLI} - </span>
                                <span className={'result'} dangerouslySetInnerHTML={{__html: a.highlight ? a.highlight.TITREFULL : a._source.TITREFULL}}/>
                        </li>) :
                    null
                articleDiv = (currentArticleSearchIndex != null) ?
                    this.parseArticle(data.articlesText.hits.hits[currentArticleSearchIndex]) :
                    null
                articlesListArrowsDiv = (data && data.articlesText && data.articlesText.hits.hits) ?
                    <div className='align-center'>
                        <span>{(data && data.articlesText && data.articlesText.hits.hits) ? data.articlesText.hits.total : ''} résultat(s)</span><br/>
                        <Icon
                            icon='circle-arrow-left'
                            color={this.state.currentResultPage > 0 ? '#394B59' : '#8A9BA8'}
                            onClick={this.changeResultPage.bind(this, 'prev')}
                            className={this.state.currentResultPage > 0 ? 'pointer' : ''}
                        />
                        <span> {currentResultPage + 1} </span>
                        <Icon
                            icon='circle-arrow-right'
                            color='#394B59'
                            onClick={this.changeResultPage.bind(this, 'next')}
                            className='pointer'
                        />
                    </div> : null
                break
            default:
                articlesListDiv = null
                articleDiv = null
                break
        }

        return (
            <div id='main'>
                <div id='header'>
                    <h1>Le Journal Officieux</h1>
                </div>
                <Tabs id='searchTabs' onChange={this.handleTabChange} selectedTabId={this.state.activeTabId}>
                    <Tab id='date' title='Recherche par Parution' panel={dateTab} />
                    <Tab id='search' title='Recherche Textuelle' panel={searchTab} />
                </Tabs>
                <div id='resultGrid'>
                    <div id='articlesListDiv'>
                        {articlesListArrowsDiv}
                        <ul>{articlesListDiv}</ul>
                    </div>
                    <div id='articleDiv'>{articleDiv}</div>
                </div>
                <div id='footer'>
                    <a href='/apropos'>Licence, Contact & Information</a>
                </div>
            </div>
        )
    }
}
