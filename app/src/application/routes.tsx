import * as React from 'react'
import {Route} from 'react-router'
import {BrowserRouter, Switch} from 'react-router-dom'

import {App} from './containers/app'
import {Apropos} from './containers/apropos'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route path='/apropos' component={Apropos}/>
        </Switch>
    </BrowserRouter>
)

export default Routes
